package main

import (
	"fmt"
	"math/rand"
	"time"
)

type word struct {
	text string
}

func (w *word) String() string {
	return w.text
}

type typist interface {
	typeWord(w *word) bool
}

type player struct {
	name           string
	correctWords   int
	incorrectWords int
}

func (p *player) typeWord(w *word) bool {
	var input string
	fmt.Printf("%s, type this word (%ds): %s\n", p.name, 5, w.text)

	ch := make(chan bool, 1)
	go func() {
		fmt.Scanln(&input)
		if input == w.text {
			ch <- true
			return
		}
		ch <- false
	}()

	select {
	case res := <-ch:
		return res
	case <-time.After(5 * time.Second):
		fmt.Println("Time's up!")
		return false
	}
}
func main() {
	rand.Seed(time.Now().UnixNano())

	words := []word{
		{text: "hello"},
		{text: "world"},
		{text: "go"},
		{text: "programming"},
		{text: "code"},
	}

	players := []*player{
		{name: "Player 1"},
		{name: "Player 2"},
	}

	for i := 0; i < 5; i++ {
		w := &words[rand.Intn(len(words))]
		fmt.Println("Word to type:", w.text)

		for _, p := range players {
			if p.typeWord(w) {
				fmt.Printf("%s typed the word correctly!\n", p.name)
				p.correctWords++
			} else {
				fmt.Printf("%s typed the word incorrectly!\n", p.name)
				p.incorrectWords++
			}
		}
	}
	var winner *player
	var loser *player
	var maxCorrectWords int
	var minCorrectWords int

	for _, p := range players {
		if p.correctWords > maxCorrectWords {
			maxCorrectWords = p.correctWords
			winner = p
		}
		if p.correctWords < minCorrectWords || minCorrectWords == 0 {
			minCorrectWords = p.correctWords
			loser = p
		}
	}
	fmt.Println("\nGame over! Results:")
	fmt.Printf("%s: %d correct words and %d incorrect words.\n", players[0].name, players[0].correctWords, players[0].incorrectWords)
	fmt.Printf("%s: %d correct words and %d incorrect words.\n", players[1].name, players[1].correctWords, players[1].incorrectWords)

	if winner == loser {
		fmt.Printf("It's a tie! Both players typed %d correct words.\n", winner.correctWords)
	} else {
		fmt.Printf("%s is the winner with %d correct words and %d incorrect words.\n", winner.name, winner.correctWords, winner.incorrectWords)
		fmt.Printf("%s is the loser with %d correct words and %d incorrect words.\n", loser.name, loser.correctWords, loser.incorrectWords)
	}
}
